# to execute run `gnuplot barchart.plt`

reset

#set term png
#set output "barchart.png"
set term png
set output "barchart.png"

set boxwidth 0.5
set style fill solid

set offset graph 0.05,0.05,0.05,0.0
set tics out nomirror
set xlabel "C version"
set ylabel "#Projects"

set xtics rotate

set datafile separator ','

#plot "bar.dat" using 2: xtic(1) with boxes lc rgb"gray" notitle
plot "bar.dat" using 2: xtic(1) with boxes lc rgb"black" notitle
