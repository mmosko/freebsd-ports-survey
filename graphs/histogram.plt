# based on http://gnuplot-surprising.blogspot.com/2011/09/statistic-analysis-and-histogram.html
# to execute run `gnuplot histogram.plt`
#
# cat histogram.dat | sort -n > histogram.dat_CROPPED then remove anything under 5000 (to remove small codebases)

reset
n=100 #number of intervals
#max=3. #max value
#min=-3. #min value

#max=921275
#min=14550
#max=19498639
#max=19498680
#max=1000000
#max=200000
#max=4000000

#max=20000000
max=2000000
min=5000

width=(max-min)/n #interval width
#function used to map a value to the intervals
hist(x,width)=width*floor(x/width)+width/2.0
#set term png #output terminal and file
#set output "histogram.png"
set term png #output terminal and file
set output "histogram.png"
set xrange [min:max]
set yrange [0:]
#to put an empty boundary around the
#data inside an autoscaled graph.
set offset graph 0.05,0.05,0.05,0.0

#set xtics min,(max-min)/10,max
set xtics min,100000,max
#set xtics min,2000000,max

set boxwidth width*0.9
#set boxwidth width*0.5
set style fill solid 0.5 #fillstyle
set tics out nomirror
set xlabel "#SLOC"
set ylabel "#Projects"

set xtics rotate

set logscale y
#set logscale x

#count and plot
#plot "histogram.dat" u (hist($1,width)):(1.0) smooth freq w boxes lc rgb"gray" notitle
plot "histogram.dat" u (hist($1,width)):(1.0) smooth freq w boxes lc rgb"black" notitle
