## Composition analysis of FreeBSD Ports collection
[Nik Sultana](http://www.cs.iit.edu/~nsultana1/), UPenn, April 2021


### About
This analysis was carried out as part of the [Pitchfork project](https://gitlab.com/pitchfork-project/about).

License: [Apache 2.0](LICENSE)

This project consists of 3 directories:

* [scripts](scripts) -- used to carry out the analysis.
* [data](data) -- generated using those scripts.
* [graphs](graphs) -- generated using that data, and used in the PriSe paper. Small codebases (i.e., fewer than 5KLOC) are cropped to avoid biasing towards smaller codebases, and to make the histogram more legible.


### How to rerun the analysis
This analysis shows:

* For ports written in C, which variants of C are being used.
* Approximate size of that port's C codebase.

This was run on FreeBSD 12.2:
```
freebsd-version -ku
12.2-STABLE
12.2-STABLE
```

First, download the distfiles for all ports. See the [FreeBSD handbook](https://docs.freebsd.org/en/books/handbook/ports/)
for more on this -- search for "`make fetch-recursive`".
To complete the download, you'll need this amount of space:
```
$ du -h -d 0 /usr/ports/distfiles/
1.7G    /usr/ports/distfiles/
```
To automate the download, run the [downall.sh](scripts/downall.sh) script.

Once you have all the distfiles, run the [analyse.sh](scripts/analyse.sh) script.
When I ran this, it took around 15 hours to complete. Results will be placed in `/tmp/results/`, and consist of the files described in the next section.

### Data

* `LOG_CVARIANTS` and `LOG_SLOC` are the raw data produced by the script.
* `RESULTS_CVARIANTS` and `RESULTS_SLOC` are produced by processing the raw data. Note that `RESULTS_SLOC` simply includes the numbers column from `LOG_SLOC`, for later generation of a histogram.
* `UNHANDLED` lists ports that were excluded by the analysis (primarily because it determined that they weren't written in C).

### Further improvements & ideas

* Add support for other languages besides C (e.g., Python, Java, etc).
* Add support for remaining compression formats, e.g., 7zip.
* Run this on different snapshots of the ports collection, to see how its composition evolved.
