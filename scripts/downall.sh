#!/bin/sh
# Nik Sultana, April 2021
#
# sudo command time ./downall.sh | tee downall.log
# NOTE: remember to first run `sudo portsnap fetch extract`
#
#    Copyright 2021 Nik Sultana
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

START=/usr/ports
for dir1 in ${START}/*; do
  if [ -d "$dir1" ]; then
    echo "Processing ${dir1}"
    for dir2 in ${dir1}/*; do
      if [ -d "${dir2}" ]; then
        echo "> Fetching ${dir2}"
        cd ${dir2}
        START_TIME=$(date)
        command time make fetch BATCH=yes > ${dir2}/downall.stdout 2> ${dir2}/downall.stderr
        echo $? > ${dir2}/downall.status
        END_TIME=$(date)
        echo "START_TIME=${START_TIME} END_TIME=${END_TIME}" > ${dir2}/downall.timing
      fi
    done
  fi
done
