#!/usr/local/bin/bash
# Nik Sultana, UPenn, April 2021
# NOTE requires sloccount
# Run with `command time ./analyse.sh | tee analyse.stdout 2> analyse.stderr`
#
#    Copyright 2021 Nik Sultana
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

TARGETDIR=/tmp/tempdir/
RESULTSDIR=/tmp/results/
UNHANDLEDLOG=${RESULTSDIR}/UNHANDLED
LOG_SLOC=${RESULTSDIR}/LOG_SLOC
LOG_CVARIANTS=${RESULTSDIR}/LOG_CVARIANTS

RESULT_SLOC=${RESULTSDIR}/RESULT_SLOC
RESULT_CVARIANTS=${RESULTSDIR}/RESULT_CVARIANTS

rm -rf ${RESULTSDIR}
mkdir ${RESULTSDIR}
touch ${UNHANDLEDLOG}

for FILE in `find /usr/ports/distfiles -type f`
do

  # Ignored path prefixes
  case "${FILE}" in
    "/usr/ports/distfiles/go"* )
       # This path prefix contains a bunch of .go files; we're looking for archives containing C code.
       continue;;
  esac

  if [ -d ${TARGETDIR} ]
  then
    chmod -R 755 ${TARGETDIR}
    rm -rf ${TARGETDIR}
  fi

  mkdir ${TARGETDIR}
  cd ${TARGETDIR}

  cp ${FILE} ${TARGETDIR}
  LOCALFILE=`basename ${FILE}`

  case "${FILE}" in
    *".tar.gz" )
       tar xzf ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
#    *".7z" )
#       tar xzf ${LOCALFILE}
#       rm ${LOCALFILE}
#
#       echo "OK: ${FILE}";;
#    *".c.Z" )
#       tar xzf ${LOCALFILE}
#       rm ${LOCALFILE}
#
#       echo "OK: ${FILE}";;
    *".zip" )
       unzip -f ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
    *".tar.xz" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
    *".tar.bz2" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
    *".tgz" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
    *".tar.lz" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}

       echo "OK: ${FILE}";;
    *".txz" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}
       echo "OK: ${FILE}";;
    *".tar" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}
       echo "OK: ${FILE}";;
    *".tar.Z" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}
       echo "OK: ${FILE}";;
    *".tbz2" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}
       echo "OK: ${FILE}";;
    *".tbz" )
       tar xf ${LOCALFILE}
       rm ${LOCALFILE}
       echo "OK: ${FILE}";;

#    *".lzh" )
#       tar xzf ${LOCALFILE}
#       rm ${LOCALFILE}
#
#       echo "OK: ${FILE}";;
     *)
       echo "${FILE}" >> ${UNHANDLEDLOG};;
  esac

  mkdir -p `dirname "${RESULTSDIR}${FILE}"`

  sloccount ${TARGETDIR} | grep "ansic:" > "${RESULTSDIR}${FILE}.sloccount"

  # We don't process further if the codebase doesn't include C
  if [ $? -eq 0 ]
  then
    #sloccount ${TARGETDIR} | grep "ansic:" | awk '{ print $2 }' > ${LOG_SLOC}
    #cat ${RESULTSDIR}${FILE}.sloccount | awk '{ print $2 }' >> ${LOG_SLOC}
    for LINE in `cat "${RESULTSDIR}${FILE}.sloccount" | awk '{ print $2 }'`
    do
      echo "${LOCALFILE}, ${LINE}" >> ${LOG_SLOC}
    done

    HAVE_STD=
    for STD in c89 c90 iso9899:1990 iso9899:199409 gnu89 gnu90 c99 iso9899:1999 gnu99 c11 iso9899:2011 gnu11 c17 iso9899:\
2017 gnu17
    do
      grep "\-std=${STD}" -R ${TARGETDIR} > /dev/null
      if [ $? -eq 0 ]
      then
        HAVE_STD=1
        echo "${STD}" >> "${RESULTSDIR}${FILE}.Cstds"
      fi
    done

    # Handle "-ansi" == "-std=c89"
    grep "\-ansi" -R ${TARGETDIR} > /dev/null
    if [ $? -eq 0 ]
    then
      HAVE_STD=1
      echo "c89" >> "${RESULTSDIR}${FILE}.Cstds"
    fi

    # If ansic>0 but no --std= then classify as "default" (i.e., gnu11)
    if [ -z ${HAVE_STD} ]
    then
      echo "default(gnu11)" >> "${RESULTSDIR}${FILE}.Cstds"
    fi

    for LINE in `cat "${RESULTSDIR}${FILE}.Cstds"`
    do
      echo "${LOCALFILE}, ${LINE}" >> ${LOG_CVARIANTS}
    done
  fi
done

awk '{ print $2 }' ${LOG_SLOC} > ${RESULT_SLOC}
echo "RESULT_SLOC=${RESULT_SLOC}"

awk '{ print $2 }' ${LOG_CVARIANTS} | sort | uniq -c | sort -nr | awk '{ print $2 ", " $1 }' > ${RESULT_CVARIANTS}
echo "RESULT_CVARIANTS=${RESULT_CVARIANTS}"

